/* This short program will determine whether an input is even or odd*/
package evenorodd;
import java.util.Scanner;
public class EvenOrOdd {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();
        System.out.print(processInt(i) + "\n");
    }
    public static String processInt(int num){
        /*
            With this method, we have two options. We could choose to have our
            'if' statement check for a remainder of '1', or a remainder of '0'.
            If the method checks for a remainder of '1', then it must also check
            for a remainder of '-1' otherwise negative numbers will give a result
            of "Even".  Using zero will eliminate the need to check
            for negative numbers.
        */
        
        if(num % 2 == 0){
            return "Even";
        }
        else {
            return "Odd";
        }
        
}
}


