/*
    The purpose of this program is to practice Integer to Character conversion.
    Each digit of an integer is split into it's own part (ex: 1234 -> 1, 2, 3, 4)
    and then squared (ex: 1^2=2, 2^2=4, 3^2=9, 4^2=16).  The squared numbers are then 
    reassembled into a combined Integer (ex:24916)
*/
package squarethedigits;

import java.util.Scanner;
public class SquareTheDigits {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int input = scan.nextInt();
        System.out.print(squareDigits(input) + "\n");
        
    }
    public static int squareDigits(int n) {
    char[] splitnums = ("" + n).toCharArray();
    String result = "";
    for(int i=0; i<splitnums.length; i++){
      int square = Character.getNumericValue(splitnums[i]);
      square *= square;
      result += "" + square;
    }
    return Integer.parseInt(result);
  }
}
